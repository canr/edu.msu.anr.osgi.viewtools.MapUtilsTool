This OSGi plugin provides and extends the Apache Commons Collections
[MapUtil](https://commons.apache.org/proper/commons-collections/javadocs/api-3.2.2/org/apache/commons/collections/MapUtils.html)
class as a [ViewTool](http://velocity.apache.org/tools/devel/). This enables
dotCMS Velocity programmers to call methods from the MapUtils class on objects
in their Velocity code.

# Installation
This plugin must be built from source and deployed to dotCMS' OSGi framework.

## Building
In order to build this plugin, use the included Gradle wrapper to run the 'jar'
task.

On Windows:
```
.\gradlew.bat jar
```

On Linux or MacOS:
```
./gradlew jar
```

The resulting .jar file should be located in './build/libs'.

## Deploying
In order to deploy the compiled plugin to dotCMS' OSGi framework, you may either
upload the plugin .jar through dotCMS' Dynamic Plugins portlet or manually place
the plugin in the dotCMS instance's Apache Felix load directory.

### Dynamic Plugins Portlet
See [dotCMS' documentation on the Dynamic Plugins portlet](https://dotcms.com/docs/latest/osgi-plugins#Portlet).

### Filesystem
```
cp ./build/libs/*.jar "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/"
```

# Usage
The MapUtilsTool can be accessed in a Velocity template using the key
'$maputils'. The MapUtils methods available can be seen in [the MapUtils Javadocs](https://commons.apache.org/proper/commons-collections/javadocs/api-3.2.2/org/apache/commons/collections/MapUtils.html).
MapUtilsTool also provides its own utility methods to make the MapUtils methods
more accessible in Velocity.

For example, a debugPrint() method is provided
which takes only a Map as an argument.

```vtl
## Create a HashMap using Velocity's map literal notation.
#set( $exampleMap = { "foo" : "bar", "baz" : 1.3 } )

## Debug print that map
$maputils.debugPrint($exampleMap)
```

Running the above snippet outputs a pretty-printed string representation of the $exampleMap, which looks like this:

```
{ foo = bar java.lang.String baz = 1.3 java.lang.Double } java.util.LinkedHashMap
```