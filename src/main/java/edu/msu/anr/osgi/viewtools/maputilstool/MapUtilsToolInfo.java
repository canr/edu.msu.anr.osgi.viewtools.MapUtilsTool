package edu.msu.anr.osgi.viewtools.maputilstool;

import org.apache.velocity.tools.view.context.ViewContext;
import org.apache.velocity.tools.view.servlet.ServletToolInfo;

/**
 * Provides information about the {@link MapUtilsTool}.
 * @author slenkeri
 */
public class MapUtilsToolInfo extends ServletToolInfo {

	/**
	 * Gets the key to which the {@link MapUtilsTool} will be mapped.
	 */
    @Override
    public String getKey () {
        return "maputils";
    }

    /**
     * Gets the scope in which the {@link MapUtilsTool} will be defined.
     */
    @Override
    public String getScope () {
        return ViewContext.APPLICATION;
    }

    /**
     * Gets the name of the {@link MapUtilsTool} class.
     */
    @Override
    public String getClassname () {
        return MapUtilsTool.class.getName();
    }

    /**
     * Instantiates a {@link MapUtilsTool}.
     */
    @Override
    public Object getInstance ( Object initData ) {

        MapUtilsTool viewTool = new MapUtilsTool();
        viewTool.init( initData );

        setScope( ViewContext.APPLICATION );

        return viewTool;
    }

}