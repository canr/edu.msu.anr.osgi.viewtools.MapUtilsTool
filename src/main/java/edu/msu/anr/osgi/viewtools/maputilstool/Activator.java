package edu.msu.anr.osgi.viewtools.maputilstool;

import com.dotcms.repackage.org.osgi.framework.BundleContext;
import com.dotmarketing.osgi.GenericBundleActivator;

/**
 * OSGi Activator for the MapUtilsTool.
 * @author slenkeri
 */
public class Activator extends GenericBundleActivator {

	/**
	 * Starts the OSGi bundle.
	 * <p>
	 * Initializes services and registers the viewtool.
	 */
    @Override
    public void start ( BundleContext bundleContext ) throws Exception {

        // Initializing services...
        initializeServices( bundleContext );

        // Registering the ViewTool service
        registerViewToolService( bundleContext, new MapUtilsToolInfo() );
    }

    /**
     * Stops the OSGi bundle.
     * <p>
     * Unregisters the viewtool.
     */
    @Override
    public void stop ( BundleContext bundleContext ) throws Exception {
        unregisterViewToolServices();
    }

}