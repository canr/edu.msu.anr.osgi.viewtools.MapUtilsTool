package edu.msu.anr.osgi.viewtools.maputilstool;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import org.apache.velocity.tools.view.tools.ViewTool;
import com.dotcms.repackage.org.apache.commons.collections.MapUtils;
import com.dotmarketing.util.Logger;

/**
 * A {@link ViewTool} that provides and extends the Apache Commons Collections
 * <a href="https://commons.apache.org/proper/commons-collections/javadocs/api-3.2.2/org/apache/commons/collections/MapUtils.html">MapUtils</a> class.
 * @author slenkeri
 * @see <a href="https://commons.apache.org/proper/commons-collections/javadocs/api-3.2.2/org/apache/commons/collections/MapUtils.html">MapUtils Javadoc</a>
 */
public class MapUtilsTool extends MapUtils implements ViewTool {

	/**
	 * Initializes the {@link ViewTool}.
	 */
	@Override
	public void init (Object initData) {
		// Nothing to initialize
	}

    /**
     * Prints a {@link Map} with type information for each value in the map.
     * @param map A map to be printed.
     * @return String representing the given map in a human-readable fashion.
     */
    public String debugPrint (Map<?,?> map) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PrintStream outPrint = new PrintStream(out);

        try
        {
            MapUtils.debugPrint(outPrint, null, map);
            return new String(out.toByteArray(), StandardCharsets.UTF_8);
        } catch (NullPointerException e) {
        	Logger.error(this, "Unable to print map to null stream.");
            return "Unable to print map to null stream.";
        } catch (Exception e) {
        	Logger.error(this, e.getMessage());
            return "Exception: " + e.getMessage();
        }
    }

}
